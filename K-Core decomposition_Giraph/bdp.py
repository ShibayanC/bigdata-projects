import numpy as np
import networkx as nx
import matplotlib.pyplot as plt
import pandas as pd

# Function to calculate the core number
def coreNumber(G):   # To calculate the core number
	# Input attribute is the graph
	if G.is_multigraph():
		raise nx.NetworkXError(
					'MultiGraph and MultiDigraph types not supported.')
	if G.number_of_selfloops()>0:
		raise nx.NetworkXError('Input graph has self loops; the core number is not defined.',
                	'Consider using G.remove_edges_from(G.selfloop_edges()).')
    if G.is_directed():
    	import itertools
    	def neighbours(v):
    		return itertools.chain.from_iterable([G.predecessors_iter(v), G.successors_iter(v)])
    else:
    	neighbours = G.neighbors_iter
    degrees = G.degree()
    
    # sort nodes by degree
    nodes = sorted(degrees, key = degrees.get)
    bin_boundaries = [0]
    curr_degree = 0
    for i, v in enumerate(nodes):
    	if degrees[v] > curr_degree:
    		bin_boundaries.extend([i]*(degrees[v] - curr_degree))
    
    node_pos = dict((v, pos) for pos, v in enumerate(nodes))
    
    # initial guesses for core is degree
    core = degrees
    nbrs = dict((v, neighbours(v))) for v in G)
    for v in nodes:
    	for u in nbrs[v]:
    		if core[u] > core[v]:
    			nbrs[u].remove(v)
    			pos = node_pos[u]
    			bin_start = bin_boundaries[core[u]]
    			node_pos[u] = bin_start
    			node_pos[nodes[bin_start]] = pos
    			nodes[bin_start], nodes[pos] = nodes[pos], nodes[bin_start]
    			bin_boundaries[core[u]]+ = 1
    			core[u]- = 1
    return core  # Returning core

# Function to calculate k_core
def k_core(G,k=None,core_number=None):  # K-Core is a maximal subgraph that contains nodes of degree K or more
    if core_number is None:
        core_number=nx.core_number(G)
    if k is None:
        k=max(core_number.values()) # max core
    nodes=(n for n in core_number if core_number[n]>=k)
    N.append(nodes)
    return G.subgraph(nodes).copy()  # Returns K-Core subgraph

# Function to calculate k_shell
def k_shell(G, k = None, core_number = None):
	# K_shell is the subgraph of nodes in the K-Core, not present in (K + 1) core
	if core_number is None:
		core_number = nx.core_number(G)
		# Max core value
	if k is None:
		k = max(core_number.values()) # Maximum core
	nodes = (n for n in core_number if core_number[n] == k)
	return G.subgraph(nodes).copy()  # Returns NetworkX graph, k-shell subgraph

# Function to calculate k_crust
def k_crust(G, k = None, core_number = None):
	# G: NetworkX graph
	# k: int, optional
	if core_number is None:
		core_number = nx.core_number(G)
	if k is None:
		k = max(core_number.values()) - 1
	nodes = (n for n in core_number if core_number[n] <= k)
	return G.subgraph(nodes).copy()  # G: NetworkX graph k-crust subgraph
	

# Function to calculate k_corona
def k_corona(G, k, core_number = None):
	# G: NetworkX directed graph
	# k: order of corona (int)
	if core_number is None:
		core_number = nx.core_number(G)
	nodes = (n for n in core_number if core_number[n] == k and len([v for v in G[n] if core_number[v] >= k]) == k)]))
	return G.subgraph(nodes).copy()  # G: NetworkX graph k-corona subgraph
	
# Function to calculate the eccentricity
def eccentricity(G, v=None, sp=None):
    # The eccentricity of a node v is the maximum distance from v to all other nodes in G.
    # Input arguments: G, directed graph
    nodes=[]
    if v is None:                # none, use entire graph 
        nodes=G.nodes() 
    elif isinstance(v, list):  # check for a list
        nodes=v
    else:                      # assume it is a single value
        nodes=[v]
    order=G.order()
    e={}
    for v in nodes:
        if sp is None:
            length=networkx.single_source_shortest_path_length(G,v)
        else:
            length=sp[v]
        try:
            L = len(length)
        except TypeError:
            raise networkx.NetworkXError('Format of "sp" is invalid.')
        e[v]=max(length.values())

    if len(e)==1: 
        return list(e.values())[0] # return single value
    else:
        return e # ecc values (dict)

# Function to calculate the diameter
def diameter(G, e = None):
	# Input argument: directed graph "G"
	if e is None:
		e = eccentricity(G)
	return max(e.values())
	
# Function to calculate the periphery 
def periphery(G, e = None):
	# Periphery is the set of nodes with eccentricty equal to diameter
	# Input arguments: directed graph
	if e is None:
		e = eccentricity(G)
	diameter = max(e.values())
	p = [v for v in e if e[v] == diameter]
	return p
	
# Function to calculate the radius
def radius(G, e = None):
	# Input arguments: directed graph
	# Computes radius: minimum eccentricity
	if e is None:
		e = eccentricity(G)
	return min(e.values())  # Returning radius
	
# Function to calculate center
def center(G, e = None):
	# Center is the eccentricity with minimum radius
	# Input arguments: directed graph
	if e is None:
		e = eccentricity(G)
	# ordering nodes by path length
	radius = min(e.values())
	p = [v for v in e if e[v] == radius]
	return p  # Returning center
		
		


 
	
# Importing dataset from SNAP	
dataSet = pd.read_csv(open("p2p-Gnutella08.data"), header = None, sep = "\t")
data = dataSet.values
print ("Size of DataSet: ", data.shape)

dataNodes = []
Nodes = []

nodesN = data.shape[0]
k = 0
for k in range(nodesN):
	x1 = data[k][0]
	x2 = data[k][1]
	dataNodes.append([x1, x2])

n1 = int(dataNodes[0][0])
n2 = int(dataNodes[0][1])
if n1 == n2:
	Nodes.append(n1)
else:
	Nodes.append(n1)
	Nodes.append(n2)

x = len(dataNodes)
k = 1
for k in range(x):
	n1 = int(dataNodes[k][0])
	n2 = int(dataNodes[k][1])
	lNodes = len(Nodes)
	j = 0
	flag = 0
	flag1 = 0
	for j in range(lNodes):
		if (int(Nodes[j]) == n1):
			flag = 1
		if (int(Nodes[j]) == n2):
			flag1 = 1
	if (flag == 0) and (flag1 == 0):
		Nodes.append(n1)
		Nodes.append(n2)
	elif (flag == 1) and (flag1 == 0):
		Nodes.append(n2)
	elif (flag == 0) and (flag1 == 1):
		Nodes.append(n1)

Nodes.sort()
print ("Total no. of Nodes: ", len(Nodes))

diG = nx.DiGraph()
diG.add_nodes_from(Nodes, label = Nodes)

x = len(dataNodes)
k = 0
for k in range(x):
	diG.add_edge(dataNodes[k][0], dataNodes[k][1])

# Code for plotting the Graph generated
plt.figure(figsize = (6, 4))
nx.draw_random(diG)
plt.title("Graph View")
plt.legend(('Vertex', 'Di-Edges'), loc = 'best')
plt.show()

# Calculating the core number
graphCN = core_number(diG)
coreNumber = []
coreNumber = graphCN  # List to save the core number

# Calculating the K-Core
find_cores=core_number
N = []
graphCore = []

cores = [1, 2, 3]  # Calculating cores for each of the values
for c in cores:
	graphKCore = k_core(diG,c)
	graphCore.append([graphKCore, c])

# Code for plotting the K-Core graph
plt.figure(figsize = (15, 6))
plt.subplot(1, 2, 1)
nx.draw(graphKCore)
plt.legend(("Node","Di-edge"), loc = "best")

plt.subplot(1, 2, 2)
plt.plot(graphKCore, '*-')
plt.xlabel("Core Values")
plt.ylabel("Node ID")
plt.legend(("Data",), loc = "best")
plt.show()

# Calculating the K-shell
KShell = []
shell = [1, 2, 3]  # Calculating shell for each of the values
for s in shell:
	graphKshell = k_shell(diG, s)
	KShell.append([graphKshell, s])
	
# Code for plotting the K-Shell
plt.figure(figsize = (15, 6))
plt.subplot(1, 2, 1)
nx.draw(graphKshell)
plt.legend(("Node","Di-edge"), loc = "best")

plt.subplot(1, 2, 2)
plt.plot(graphKshell, '*')
plt.xlabel("Shell Values")
plt.ylabel("Node ID")
plt.legend(("Data",), loc = "best")
plt.show()

# Code for calculating the K-Crust
crust = [1, 2, 3]
KCrust = []
for cr in crust:
	graphKcrust = k_crust(diG, cr)
	KCrust.append([graphKcrust, cr])
	
# Code for printing the K-Crust
plt.figure(figsize = (15, 6))
plt.subplot(1, 2, 1)
nx.draw(graphKcrust)
plt.legend(("Node","Di-edge"), loc = "best")

plt.subplot(1, 2, 2)
plt.plot(graphKcrust, '*')
plt.xlabel("Crust Values")
plt.ylabel("Node ID")
plt.legend(("Data",), loc = "best")
plt.show()

# Code for calculating the K-Corona
corona = [1, 2, 3]
KCorona = []
for cor in corona:
	graphKcorona = k_corona(diG, cor)
	KCorona.append([graphKcorona, cor])
	
# Code for printing the K-Corona
plt.figure(figsize = (6, 4))
nx.draw(graphKcorona)
plt.legend(("Node",), loc = "best")
plt.title("Corona for the graph")
plt.show()

# Code for calculating the diameter
diaCal = diameter(diG)
print ("Printing the diameters: ", diaCal)

# Code for calculating the periphery
perCal = periphery(diG)
print ("Printing the periphery: ", perCal)

# Code for calculating the radius
radCal = radius(diG)
print ("Printing the radius: ", radCal)

# Code for calculating the center
cenCal = center(diG)
print ("Printing the center: ", cenCal)	



