import networkx as nx
import numpy as np
import pandas as pd
import sys
import os
import matplotlib.pyplot as plt
os.system("clear")

# Function to calculate the core number
def core_number(G):   # Function to determine the core_number
    # Maximal subgraph containing nodes of degree 'k'
    # G: NetworkX graph, directed
    if G.is_multigraph():
        raise nx.NetworkXError(
                'MultiGraph and MultiDiGraph types not supported.')

    if G.number_of_selfloops()>0:
        raise nx.NetworkXError(
                'Input graph has self loops; the core number is not defined.',
                'Consider using G.remove_edges_from(G.selfloop_edges()).')

    if G.is_directed():
        import itertools
        def neighbors(v):
            return itertools.chain.from_iterable([G.predecessors_iter(v),
                                                  G.successors_iter(v)])
    else:
        neighbors=G.neighbors_iter
    degrees=G.degree()
    
    # sort nodes by degree
    nodes=sorted(degrees,key=degrees.get)
    bin_boundaries=[0]
    curr_degree=0
    for i,v in enumerate(nodes):
        if degrees[v]>curr_degree:
            bin_boundaries.extend([i]*(degrees[v]-curr_degree))
            curr_degree=degrees[v]
    node_pos = dict((v,pos) for pos,v in enumerate(nodes))
    
    # initial guesses for core is degree
    core=degrees
    nbrs=dict((v,set(neighbors(v))) for v in G)
    for v in nodes:
        for u in nbrs[v]:
            if core[u] > core[v]:
                nbrs[u].remove(v)
                pos=node_pos[u]
                bin_start=bin_boundaries[core[u]]
                node_pos[u]=bin_start
                node_pos[nodes[bin_start]]=pos
                nodes[bin_start],nodes[pos]=nodes[pos],nodes[bin_start]
                bin_boundaries[core[u]]+=1
                core[u]-=1
    return core  # Returns core values as dictionary

# Function to calculate the K-Core
def k_core(G,k=None,core_number=None):  # A k-core is a maximal subgraph that contains nodes of degree k or more
    if core_number is None:
        core_number=nx.core_number(G)
    if k is None:
        k=max(core_number.values()) # max core
    nodes=(n for n in core_number if core_number[n]>=k)
    N.append(nodes)
    return G.subgraph(nodes).copy()  # Returns k-core subgraph

# Function to calculate the K-Shell
def k_shell(G,k=None,core_number=None):
    # k_shell is the subgraph of nodes in the k-core but not present in the (k+1) - core
    if core_number is None:
        core_number=nx.core_number(G)
    if k is None:
        k=max(core_number.values()) # max core
    nodes=(n for n in core_number if core_number[n]==k)
    return G.subgraph(nodes).copy()  # Returns NetworkX graph, k-shell subgraph
    
# Function to calculate the K-Corona
def k_corona(G, k, core_number=None):
    # G: A graph or directed graph
    # k: order of the corona (int)
    if core_number is None:
        core_number = nx.core_number(G)
    nodes = (n for n in core_number
             if core_number[n] == k
             and len([v for v in G[n] if core_number[v] >= k]) == k)
    return G.subgraph(nodes).copy()  # G : NetworkX graph k-corona subgraph

# Function to calculate the eccentricity
def eccentricity(G, v=None, sp=None):
    # The eccentricity of a node v is the maximum distance from v to all other nodes in G.
    # Input arguments: G, directed graph
    nodes=[]
    if v is None:                # none, use entire graph 
        nodes=G.nodes() 
    elif isinstance(v, list):  # check for a list
        nodes=v
    else:                      # assume it is a single value
        nodes=[v]
    order=G.order()
    e={}
    for v in nodes:
        if sp is None:
            length=nx.single_source_shortest_path_length(G,v)
        else:
            length=sp[v]
        try:
            L = len(length)
        except TypeError:
            raise nx.NetworkXError('Format of "sp" is invalid.')
        e[v]=max(length.values())

    if len(e)==1: 
        return list(e.values())[0] # return single value
    else:
        return e # ecc values (dict)

# Function to calculate the diameter
def diameter(G, e=None):
    # Input argument: directed graph 'G'
    if e is None:
        e=eccentricity(G)
    #print (e)
    return e, max(e.values())  # Graph diameter (int) with max eccentricity
    
# Function to calculate the periphery    
def periphery(G, e=None):
    # Periphery is the set of nodes with eccentricty equal to diameter
    # Input arguments: directed graph
    if e is None:
        e=eccentricity(G)
    diameter=max(e.values())
    p=[v for v in e if e[v]==diameter]
    return p

# Function to calculate the radius   
def radius(G, e=None):
    # Input arguments: directed graph
    # Computes radius: minimum eccentricity
    if e is None:
        e=eccentricity(G)
    return min(e.values())

# Function to calculate the center
def center(G, e=None):
    # Center is the eccentricity with minimum radius
    # Input arguments: directed graph
    if e is None:
        e=eccentricity(G)
    # order the nodes by path length
    radius=min(e.values())
    p=[v for v in e if e[v]==radius]
    return p

# Calculating the graph computation features, by selecting cluster head    
def calcJ(data,centers):
    diffsq = (centers[:,np.newaxis,:] - data)**2
    return np.sum(np.min(np.sum(diffsq,axis=2), axis=0))

# K-Means algorithm to calculate the cluster head for the network
def kmeans(data, k, n):
    # Input arguments: data: Node positions; k: No of super nodes; n: Nos of Iterations
    # Initialize centers and list J to track performance metric
    centers = data[np.random.choice(range(data.shape[0]),k,replace=False), :]
    J = []
    # Repeat n times
    for iteration in range(n):
        # Calculating squared distance
        sqdistances = np.sum((centers[:,np.newaxis,:] - data)**2, axis=2)
        closest = np.argmin(sqdistances, axis=0) # Calculating the closest ones
        # Calculaing J, optimized position and append to list J
        J.append(calcJ(data,centers))
        # Update cluster centers
        for i in range(k):
            centers[i,:] = data[closest==i,:].mean(axis=0)
            
    # Calculate J one final time and return results
    J.append(calcJ(data,centers))
    return centers,J,closest,sqdistances
    
# Main function
def main():
	# Reading from the dataset
	dataSet = pd.read_csv(open("p2p-Gnutella08.data"), header = None, sep = '\t')
	data = dataSet.values
	print ("Dimension of Data-Set: ",data.shape)

	dataNode = []  # To save the edges
	Nodes = []  # To save the nodes
	
	x = data.shape[0]
	k = 0
	for k in range(50):
		x1 = data[k][0]
		x2 = data[k][1]
		dataNode.append([x1, x2])

	n1 = int(dataNode[0][0])
	n2 = int(dataNode[0][1])
	if (n1 == n2):
		Nodes.append(n1)
	else:
		Nodes.append(n1)
		Nodes.append(n2)

	x = len(dataNode)
	k = 1
	for k in range(x):
		n1 = int(dataNode[k][0])
		n2 = int(dataNode[k][1])
	lnodes = len(Nodes)
	j = 0
	flag = 0
	flag1 = 0
	for j in range(lnodes):
		if (int(Nodes[j]) == n1):
			flag = 1
		if (int(Nodes[j]) == n2):
			flag1 = 1
		if (flag == 0) and (flag1 == 0):
			Nodes.append(n1)
			Nodes.append(n2)
		elif (flag == 1) and (flag1 == 0):
			Nodes.append(n2)
		elif (flag == 0) and (flag1 == 1):
			Nodes.append(n1)
	
	Nodes.sort()  # Saving the node values
	print ("Total Nos of Nodes: ", len(Nodes))
	
	# Making an object 'diG' to evaluate the Graph function
	diG = nx.DiGraph()
	diG.add_nodes_from(Nodes, label = Nodes)
	
	x = len(dataNode)
	k = 0
	for k in range(x):
		diG.add_edge(dataNode[k][0], dataNode[k][1], color = 'red')
	
	# Plotting the graph
	nx.draw_random(diG)
	plt.title("Graph View")
	plt.savefig("graph.png")
	plt.show()
	
	# Calculating the K-Core
	Core = [1, 2, 3]
	for core in Core:
		graphKCore = k_core(diG,core)
		plt.figure(figsize = (6, 4))
		nx.draw(graphKCore)
		
	# Calculating the K-Shell
	Shell = [1, 2, 3]
	for shell in Shell:
		graphKshell = k_shell(diG, shell)
		plt.figure(figsize = (6, 4))
		nx.draw(graphKshell)
		
	# Calculating the K-Corona
	Corona = [1, 2, 3]
	for corona in Corona:
		graphKcorona = k_corona(diG, corona)
		plt.figure(figsize = (6, 4))
		nx.draw(graphKcorona)
	
	# Code for calculating the diameter
	diaCal = diameter(diG)
	print ("Printing the diameters: ", diaCal)

	# Code for calculating the periphery
	perCal = periphery(diG)
	print ("Printing the periphery: ", perCal)
	
	# Code for calculating the radius
	radCal = radius(diG)
	print ("Printing the radius: ", radCal)
	
	# Code for calculating the center
	cenCal = center(diG)
	print ("Printing the center: ", cenCal)	

	# Calculating the computation of the network
	clusters = [10, 20, 30, 40, 50]  # Fixing the number of clusters for computation
	itern = [20, 25, 30, 35, 40, 50]  # Fixing the number of iterations for each center
	for cen in clusters:
		for itr in itern:
			c, J, close, sqdistances = kmeans(data, cen, itr)  # Calculating the centers, J, squared distance
			print (c)  # Calculating the cluster heads
			plt.plot(J)  # Plotting the optimization behaviour


# Calling the main function
if __name__ == "__main__":
	main()
