#########Main code jahan type define kar rahhi hoon and funcgtion call kar rahi hoon

for each in constant.GRAPH.nodes():
        constant.ALL_NODES[i] = node.make_node(int(each))
        constant.GRAPH.node[each]['vector_cordinates_original'] = constant.ALL_NODES[i].vector_cordinates_original
        constant.GRAPH.node[each]['node_id'] = constant.ALL_NODES[i].node_id
        constant.GRAPH.node[each]['type'] = constant.ALL_NODES[i].type
        constant.GRAPH.node[each]['paths'] = constant.ALL_NODES[i].paths
        chosen_anchor_node = constant.ANCHOR_NODES[utility.find_closest_anchor_node(each)]
        constant.GRAPH.node[each]['chosen_anchor_node'] = chosen_anchor_node
        
        i = i+1
    utility.draw_display_graph()


##############Function to draw and display the network

def draw_display_graph(display_VC = False):
    plt.close()
    pos = nx.graphviz_layout(constant.GRAPH)
    color_map = {'Anchor_node':'y', 'Regular_node':'b', 'Changed_VC_node':'r', 'deflated' : 'green', None: 'b'} 
    nx.draw_networkx_nodes(constant.GRAPH, pos, node_color=[color_map[constant.GRAPH.node[each]['type']] for each in constant.GRAPH.nodes()])
    nx.draw_networkx_edges(constant.GRAPH, pos)
    labels = {}
    for each in constant.GRAPH.nodes():
        labels[each] = each
    nx.draw_networkx_labels(constant.GRAPH, pos, labels)
    if display_VC:
        for each_node in constant.GRAPH.nodes():
            x,y=pos[each_node]
            plt.text(x, y+25, s=str(constant.GRAPH.node[each_node]['vector_cordinates_original']), fontsize = 8, horizontalalignment='center')
            if constant.GRAPH.node[each_node]['type'] is 'Changed_VC_node' or constant.GRAPH.node[each_node]['type'] is 'deflated':
                plt.text(x, y-35, s=str(constant.GRAPH.node[each_node]['vector_cordinates_changed']), fontsize = 8, color = 'red', horizontalalignment='center')

    plt.show(block = False)




