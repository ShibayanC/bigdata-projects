#!/usr/bin/env python
import sys

#line = "[01/Jul/1995:00:00:01"

d1 = int(sys.argv[1]) # Start day
d2 = int(sys.argv[2]) # End day
t1 = int(sys.argv[3]) # Start hr
t2 = int(sys.argv[4]) # End hr

for line in sys.stdin:
	words = line.split()
	x = 0
	x = len(words)	
	if ((x >= 10) and (x <= 13)):
		newLine = words[3]
		w = newLine.split(":")
		date = w[0][1:]
		hr = w[1]
		dw = date.split("/")
		day = int(dw[0])
		if ((day >= d1) and (day <= d2)):
			l1 = date + "-" + hr
			value = 1
		else:
			l1 = 'Error'
			value = 0
	elif ( x == 17 ):
		newLine = words[3]
		w = newLine.split(":")
		date = w[0][1:]
		hr = w[1]
		dw = date.split("/")
		day = int(dw[0])
		if ((day >= d1) and (day <= d2)):
			l1 = date + "-" + hr
			value = 1
		else:
			l1 = 'Error'
			value = 0
	else:
		l1 = 'Error'
		value = 0
	print("%s\t%d" % (l1, value))
