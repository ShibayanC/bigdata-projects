#!/usr/bin/env python
     
import sys
n=int(sys.argv[1])
last_key = None
running_total = 0
i=1
for input_line in sys.stdin:
   	if i <= n:
		input_line = input_line.strip()
		this_key, value = input_line.split("\t", 1)
		this_key=int(this_key)
		print( "%s\t%d" % (value, this_key) )
	i=i+1
