import numpy as np
import pandas as pd
import os
import math
import sys
import random
import matplotlib.pyplot as plt
os.system('clear')

def average(x):
    assert len(x) > 0
    return float(sum(x)) / len(x)

def pearson_def(x, y):
    assert len(x) == len(y)
    n = len(x)
    assert n > 0
    avg_x = average(x)
    avg_y = average(y)
    diffprod = 0
    xdiff2 = 0
    ydiff2 = 0
    for idx in range(n):
        xdiff = x[idx] - avg_x
        ydiff = y[idx] - avg_y
        diffprod += xdiff * ydiff
        xdiff2 += xdiff * xdiff
        ydiff2 += ydiff * ydiff

    return diffprod / math.sqrt(xdiff2 * ydiff2)

errDH = pd.read_csv(open('errdate.txt'), header = None, sep = '\t')
eDH = errDH.values
ys = eDH[:,1:2]
print ("Size of Error-Data set: ",ys.shape)

ipError = pd.read_csv(open('ipdayerr.txt'), header = None, sep = '\t')
ipE = ipError.values
xs = ipE[:,1:2]
print ("Size of IP-Error Data set: ",xs.shape)

sampleSize = int(input("Enter no. of samples (max 29): "))
samples = random.sample(range(29), sampleSize)

k = 0
X = []
Y = []

for k in samples:
	X.append(xs[k])
	Y.append(ys[k])

coeffErAcc = pearson_def(X, Y)
coeffErAcc = abs(coeffErAcc) * 10
print ("Pearson Co-efficient for Access Rate w.r.t Error Rate: ", coeffErAcc)
