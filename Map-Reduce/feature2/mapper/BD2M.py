#!/usr/bin/env python
import sys

for line in sys.stdin:
	words = line.split()
	x = 0
	x = len(words)
	if ((x >= 9) and (x <= 13)):
		accType = int(words[x - 2])
		if ((accType >= 200) and (accType <300)):
			size = str(words[x - 1])
			if size.isdigit():
				s = int(words[x - 1])
				newLine = words[3]
				w = newLine.split(":")
				dat = w[0][1:]
				hr = w[1]
				l1 = dat +"-"+ hr
				value = s
			else:
				l1 = 'Error'
				value = 1
		else:
			l1 = 'Error'
			value = 1
	else:
		l1 = 'Error:Undefined'
		value = 1
	print("%s\t%d" % (l1, value))
