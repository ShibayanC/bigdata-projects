#!/usr/bin/env python
import sys

for line in sys.stdin:
	words = line.split('\t')
	value = words[0]
	l1 = int(words[1])
	print("%d\t%s" % (l1, value))
