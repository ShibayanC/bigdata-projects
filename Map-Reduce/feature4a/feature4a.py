import numpy as np
import pandas as pd
import os
import math
import sys
import random
import matplotlib.pyplot as plt
os.system('clear')

def average(x):
    assert len(x) > 0
    return float(sum(x)) / len(x)

def pearson_def(x, y):
    assert len(x) == len(y)
    n = len(x)
    assert n > 0
    avg_x = average(x)
    avg_y = average(y)
    diffprod = 0
    xdiff2 = 0
    ydiff2 = 0
    for idx in range(n):
        xdiff = x[idx] - avg_x
        ydiff = y[idx] - avg_y
        diffprod += xdiff * ydiff
        xdiff2 += xdiff * xdiff
        ydiff2 += ydiff * ydiff

    return diffprod / math.sqrt(xdiff2 * ydiff2)

dataAccessSet = pd.read_csv(open('accessrate.data'), header = None, sep = '\t')
dataA = dataAccessSet.values
ys = dataA[:,1:2]
print ("Size of Access Data set: ",ys.shape)

dataErrorSet = pd.read_csv(open('errorrate.data'), header = None, sep = '\t')
dataE = dataErrorSet.values
xs = dataE[:,1:2]
print ("Size of Error Data set: ",xs.shape)

dataSizeSet = pd.read_csv(open('sizerate.data'), header = None, sep = '\t')
dataS = dataSizeSet.values
zs = dataS[:,1:2]
print ("Size of Data Size: ",zs.shape)

sampleSize = int(input("Enter no. of samples (max 650): "))
samples = random.sample(range(652), sampleSize)
k = 0
X = []
Y = []
Z = []
for k in samples:
	X.append(xs[k])
	Y.append(ys[k])
	Z.append(zs[k])

coeffErAcc = pearson_def(X, Y)
print ("Pearson Co-efficient for Access Rate w.r.t Error Rate: ", coeffErAcc)
coeffErSize = pearson_def(X, Z)
print ("Pearson Co-efficient for Size of Reply w.r.t Error Rate: ", coeffErSize)
coeffAccSize = pearson_def(Y, Z)
print ("Pearson Co-efficient for Acces Rate w.r.t Size of Reply: ", coeffAccSize)

d = np.hstack((X, Y, Z)).T
d = d.T
print (d.shape)

plt.figure(figsize = (8,6))
x = d[:,0:1]
order = np.argsort(x[:,0], axis = 0)
y = d[:,1:2]
z = d[:,2:3]

plt.subplot(211)
plt.plot(x[order], y[order], 'o-')
plt.xlabel('Error Rate')
plt.ylabel('Access Rate')

plt.subplot(212)
plt.plot(x[order], z[order], 'o-')
plt.xlabel('Error Rate')
plt.ylabel('Size of Reply')
plt.show()

